import {OUTSIDE_KEY, responseHandler} from "./utils";

const GAME_STATE_KEY = 'gameState';

function GameRepository(socketIoServer, httpClient) {

    const gamesServiceUrl = `games`;

    const gameBroadcast = new GameBroadcast(socketIoServer, httpClient);

    this.updateState = (id) => {
        httpClient.get(`${gamesServiceUrl}/${id}`,
            responseHandler((response) => {
                this.update(id, JSON.parse(response));
            }));
    }

    this.update = (id, gameState) => {
        gameBroadcast.update(id, gameState);
    };

    this.join = (id, socketClient) => {
        new GameUser(socketClient).join(id);
    }

    this.startGame = (roomId, accessToken) => {

        const options = {
            url: `${gamesServiceUrl}`,
            headers: {
                'Authorization': `Bearer ${accessToken}`
            },
            json: {
                id: roomId,
                assetId: 'asset_truco',
            }
        };

        httpClient.post(options, responseHandler((body) => {
            this.update(roomId, body)
        }));
    };
}

function GameBroadcast(socketIoServer) {

    this.update = (gameId, gameState) => {
        socketIoServer.to(`${GAME_STATE_KEY} ${gameId}`).emit(GAME_STATE_KEY, gameState);
    };
}

function GameUser(socketIoClient) {
    this.join = (id) => {
        socketIoClient.leave(OUTSIDE_KEY);
        socketIoClient.join(`${GAME_STATE_KEY} ${id}`)
    };
}

export {
    GameRepository
}