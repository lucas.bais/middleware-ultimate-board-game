import {IdentityRepository} from "./identity";
import {responseHandler} from "./utils";
import {RoomRepository} from "./rooms";
import {GameRepository} from "./game";

const SERVICES_BASE_URL = process.env.SERVICE_URL || 'http://localhost:8080/';
const httpClient = require('request').defaults({
    baseUrl: SERVICES_BASE_URL
});

const ROOM_SERVICE_PATH = `rooms`;
const GAME_SERVICE_PATH = `games`;

const SOCKET_PORT = process.env.PORT || 8888;
const socketIoServer = require('socket.io')();
socketIoServer.listen(SOCKET_PORT);

const identityRepository = new IdentityRepository();
const roomRepository = new RoomRepository(socketIoServer, httpClient);
const gameRepository = new GameRepository(socketIoServer, httpClient);

socketIoServer.on('connection', (client) => {
    identityRepository.add(client);
    roomRepository.broadcastRooms();
    listenOnConnection(client);
});

function listenOnConnection(client) {

    client.on('joinRoom', ({id, password, userId, accessToken}) => {
        ifUserIsNotGuestDoTheThing(client, {id, password, userId, accessToken});
    });

    client.on('startGame', ({roomId, accessToken}) => {
        gameRepository.startGame(roomId, accessToken);
    });

    client.on('action', ({action: action, gameId: gameId, accessToken: accessToken, payload}) => {

        console.log("paylod action", payload)

        if ("objectId" in payload) {

            console.log("action over object")

            const options = {
                url: `${GAME_SERVICE_PATH}/${gameId}/objects/${payload.objectId}/${action}`,
                headers: {
                    'Authorization': `Bearer ${accessToken}`
                },
                json: payload
            };

            httpClient.put(options, responseHandler((response) => {
                gameRepository.update(gameId, response);
            }));
        }

        if ("zoneId" in payload) {
            console.log("action over zone")

            const options = {
                url: `${GAME_SERVICE_PATH}/${gameId}/zones/${payload.zoneId}/${action}`,
                headers: {
                    'Authorization': `Bearer ${accessToken}`
                },
                json: payload
            };

            httpClient.put(options, responseHandler((response) => {
                gameRepository.update(gameId, response);
            }));
        }
    });

}

function ifUserIsNotGuestDoTheThing(socketClient, {id, password, userId, accessToken}) {

    httpClient.get(`${ROOM_SERVICE_PATH}/${id}`,
        responseHandler((body) => {
            const room = JSON.parse(body);
            const clientIsGuest = room.guests.find(g => g.id === userId);
            if (!clientIsGuest) {
                console.log("only join room - is not guest");
                joinRoom(socketClient, {id, password, accessToken});
                return;
            }

            console.log("re-join room - it was a guest");
            roomRepository.join(id, socketClient);
            gameRepository.join(id, socketClient);
            roomRepository.update(id, room);
            gameRepository.updateState(id);
            roomRepository.broadcastRooms(); // Update outsides
        }, socketClient));
}

function joinRoom(client, {id, password, accessToken}) {

    const options = {
        url: `${ROOM_SERVICE_PATH}/${id}/join`,
        headers: {
            'Authorization': `Bearer ${accessToken}`
        },
        json: {
            password: password
        }
    };

    httpClient.put(options, responseHandler((room) => {
        roomRepository.join(id, client);
        gameRepository.join(id, client);
        roomRepository.update(id, room);
        gameRepository.updateState(id);
        roomRepository.broadcastRooms(); // Update outsides
    }, client));
}
