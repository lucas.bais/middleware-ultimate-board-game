//const defaultErrorCb = (client, body) => socketIoServer.to(client.id).emit('error', body);
const responseHandler = function (successCallback, client = null, errorCallback = null) {
    return (error, res, body) => {

        console.log('Handler error, res, body', error, body);

        if (res == null) return;

        if (res.statusCode >= 300 || res.statusCode < 200) {
            if (client != null && errorCallback != null) {
                errorCallback(client, body);
            }
        } else {
            successCallback(body);
        }
    };
};
const OUTSIDE_KEY = 'outside';

export {responseHandler, OUTSIDE_KEY};