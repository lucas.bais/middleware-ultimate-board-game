import {OUTSIDE_KEY} from "./utils";

function IdentityRepository() {

    this.add = (socketClient) => {
        socketClient.leaveAll();
        socketClient.join(OUTSIDE_KEY);
        socketClient.on('disconnect', function () {
            socketClient.leaveAll()
        });
    };
}

export {IdentityRepository};