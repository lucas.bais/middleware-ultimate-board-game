# Created by Butter and Children (c)

## Middleware

NodeJS socket.io communication layer.

- `cd middleware`
- install `nvm` <https://github.com/nvm-sh/nvm>
- nvm env variables

  ```bash
  export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
  [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
  ```

- `nvm use` to use `.nvmrc` version
- `npm install` the first time
- `npm start` starts the middleware running on por `:8000`
- communicate with client using socket.io
- TODO fordward `emit` and `on` to kotlin