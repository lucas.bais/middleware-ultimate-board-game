import {OUTSIDE_KEY, responseHandler} from "./utils";

const ROOMS_KEY = 'rooms';
const ROOM_KEY = 'room';

function RoomRepository(socketIoServer, httpClient) {

    const roomBroadcast = new RoomBroadcast(socketIoServer);
    const roomListBroadcast = new RoomListBroadcast(socketIoServer, httpClient);

    this.broadcastRooms = () => roomListBroadcast.all();

    this.join = (id, socketIoClient) => {
        socketIoClient.join(`${ROOM_KEY} ${id}`);
    }

    this.update = (id, room) => {
        roomBroadcast.update(id, room);
    };
}

function RoomListBroadcast(socketIoServer, httpClient) {

    const ROOM_SERVICE_URL = `rooms`;

    this.all = () => {
        httpClient.get(ROOM_SERVICE_URL, responseHandler((body) => {
            socketIoServer.to(OUTSIDE_KEY).emit(ROOMS_KEY, JSON.parse(body));
        }));
    };
}

function RoomBroadcast(socketIoServer) {
    this.update = (id, message) => {
        socketIoServer.to(`${ROOM_KEY} ${id}`).emit(ROOM_KEY, message);
    };
}

export {RoomRepository};